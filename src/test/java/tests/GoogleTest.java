package tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import base.Base;

/**
 * 
 * 
 * Example Selenium test.
 *
 */
public class GoogleTest extends Base {

	/**
	 * Example test, load google and search for the item from the datafile
	 * "criteria1".
	 * @throws InterruptedException 
	 */
	@Test
	public void exampleGoogleTest1() throws InterruptedException {
		getLogger().info("============ GoogleTest1 Start ============");

		// given
		getDriver().get("http://www.google.com");
		WebElement element = getDriver().findElement(By.name("q"));
		element.sendKeys("Cheese");

		// when
		element.submit();

		Thread.sleep(3000);
		
		// then
		assertTrue(getDriver().getTitle().contains("Google"));
		getLogger().info("Page title is: " + getDriver().getTitle());

		getLogger().info("============ GoogleTest1 Complete ============");
	}

	/**
	 * Example test, load google and search for the item from the datafile
	 * "criteria2".
	 * @throws InterruptedException 
	 */
	@Test
	public void exampleGoogleTest2() throws InterruptedException {
		getLogger().info("============ GoogleTest2 Start ============");

		// given
		getDriver().get("http://www.google.com");
		WebElement element = getDriver().findElement(By.name("q"));
		element.sendKeys("Mouse");

		// when
		element.submit();
		
		Thread.sleep(3000);

		// then
		assertTrue(getDriver().getTitle().contains("Google"));
		getLogger().info("Page title is: " + getDriver().getTitle());

		getLogger().info("============ GoogleTest Complete ============");
	}

}
