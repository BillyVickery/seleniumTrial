package base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Abstract class to implement Selenium. Sets up a WebDriver based upon the parameter given in the constructor. To use: inherit from this class, create a
 * default constructor calling the one constructor in this class, passing in the browser type and a path to the driver.
 *
 */
public abstract class Base
{
    /** Selenium WebDriver object. */
    private WebDriver driver;

    /** Time out to wait for an operation on the web driver to complete. */
    private int timeout;

    /** Logger for the subclass. */
    private final Logger log = LoggerFactory.getLogger(getClass());

    /** Logger for this base class. */
    private final Logger baseLog = LoggerFactory.getLogger(Base.class);

    /** path to physical driver file. */
    private String driverPath;

    /** The browser as defined by the Enum {@link Browser}. */
    private Browser browser;

    /** Internal structure to hold input data. */
    private Map<Object, Object> data = null;

    /** Holds all the xpaths for the menu. */
    private Map<Object, Object> pageToData = null;

    /** Holds all the xpaths for the menu. */
    private Map<Object, Object> screenData = null;

    /** Holds all the xpaths for the menu. */
    private Map<Object, Object> notificationData = null;

    /** Holds all the xpaths for the menu. */
    private Map<Object, Object> shipmentData = null;

    /** Base URL, used at create a cookie to login */
    private String baseURL;

    private String gridURL = "http://esdch-svwg-bld:4444/wd/hub";

    private static ChromeDriverService service;

    /**
     * Default Constructor - use VM arg to either go local firefox (default) or -Dbrowser=IR_REMOTE to use Selenium Grid via Jenkins (this is for Jenkins builds
     * to auto run regression tests). Grid URL is also hard coded but can be overridden with the -DgridURL=<new url> VM arg.
     */
    public Base()
    {
        if ((System.getProperty("browser") != null) && (System.getProperty("browser").equalsIgnoreCase("IR_REMOTE")))
        {
            this.driverPath = null;
            this.browser = Browser.IE_REMOTE;
        }
        else
        {
            this.driverPath = null;
            this.browser = Browser.FIREFOX;
        }

        if (System.getProperty("gridURL") != null)
        {
            gridURL = System.getProperty("gridURL");
        }
    }

    /**
     * Get the WebDriver.
     * 
     * @return The current web driver.
     */
    protected WebDriver getDriver()
    {
        return driver;
    }

    /**
     * Get the logger.
     * 
     * @return The logger of the subclass.
     */
    protected Logger getLogger()
    {
        return log;
    }

    /**
     * Get the value of one data item.
     * 
     * @param key
     *            The key to the key/value pair.
     * @return Value defined by the key.
     */
    protected String getData(final String key)
    {
        if (null == data)
        {
            baseLog.info("data is null. Not populating the map");
        }
        if (data.containsKey(key))
        {
            return (String) data.get(key);
        }
        else
        {
            throw new IllegalArgumentException("Key not found in data: " + key);
        }
    }

    /**
     * Get the value of one data item.
     * 
     * @param key
     *            The key to the key/value pair.
     * @return Value defined by the key.
     */
    protected String getPageTo(final String key)
    {
        if (null == pageToData)
        {
            baseLog.info("pageToData is null. Not populating the map");
        }
        if (pageToData.containsKey(key))
        {
            return (String) pageToData.get(key);
        }
        else
        {
            throw new IllegalArgumentException("Key not found in pageToData: " + key);
        }
    }

    /**
     * Get the value of one data item.
     * 
     * @param key
     *            The key to the key/value pair.
     * @return Value defined by the key.
     */
    protected String getScreenName(final String key)
    {
        if (null == screenData)
        {
            baseLog.info("screenData is null. Not populating the map");
        }
        if (screenData.containsKey(key))
        {
            return (String) screenData.get(key);
        }
        else
        {
            throw new IllegalArgumentException("Key not found in screenData: " + key);
        }
    }

    /**
     * Get the value of one data item. This is for NotificationBase
     * 
     * @param key
     *            The key to the key/value pair.
     * @return Value defined by the key.
     */
    protected String getValueNotification(final String key)
    {
        if (null == notificationData)
        {
            baseLog.info("notificationData is null. Not populating the map");
        }
        if (notificationData.containsKey(key))
        {
            return (String) notificationData.get(key);
        }
        else
        {
            throw new IllegalArgumentException("Key not found in notificationData: " + key);
        }
    }

    /**
     * Get the value of one data item. This is for ShipmentBase
     * 
     * @param key
     *            The key to the key/value pair.
     * @return Value defined by the key.
     */
    protected String getValueShipment(final String key)
    {
        if (null == shipmentData)
        {
            baseLog.info("shipmentData is null. Not populating the map");
        }
        if (shipmentData.containsKey(key))
        {
            return (String) shipmentData.get(key);
        }
        else
        {
            throw new IllegalArgumentException("Key not found in shipmentData: " + key);
        }
    }

    /**
     * Get the browser operation timeout.
     * 
     * @return number of seconds that the timeout is set to.
     */
    protected int getTimeout()
    {
        return timeout;
    }

    /**
     * Set the browser operation timeout.
     * 
     * @param timeout
     *            the number of seconds.
     */
    protected void setTimeout(final int timeout)
    {
        this.timeout = timeout;
    }

    private void outputLogStatus()
    {
        System.out.format("Logging levels.  Error:%b, Warn:%b, Info:%b, Debug:%b, Trace:%b\n", baseLog.isErrorEnabled(), baseLog.isWarnEnabled(),
                baseLog.isInfoEnabled(), baseLog.isDebugEnabled(), baseLog.isTraceEnabled());

    }

//    /**
//     * BeforeClass for Google.
//     *
//     * @throws IOException
//     */
//    @BeforeClass
//    public static void createAndStartService() throws IOException
//    {
//        service = new ChromeDriverService.Builder().usingDriverExecutable(new File("D:\\Selenium\\chromedriver.exe")).build();
//        service.start();
//    }
//
//    /**
//     * AfterClass for Google.
//     */
//    @AfterClass
//    public static void createAndStopService()
//    {
//        service.stop();
//    }

    /**
     * Set up the driver and the data.
     */
    @Before
    public void before()
    {
        try
        {
            outputLogStatus();
            baseLog.info("============ Start Test ============");

            if (null != driverPath)
            {
                System.setProperty(browser.toString(), driverPath);
            }
            // If you comment out the line below then it forces it to run in the remote desktop.
            // browser = Browser.IE_REMOTE;
            browser = Browser.CHROME;
            switch (browser)
            {
            case CHROME:
                driver = new RemoteWebDriver(service.getUrl(), DesiredCapabilities.chrome());
                // driver = new RemoteWebDriver("http://127.0.0.1:9515", DesiredCapabilities.chrome());
                break;
            case FIREFOX:
                driver = new FirefoxDriver();
                break;
            case IE:
                driver = new InternetExplorerDriver();
                break;
            case IE_REMOTE:
                DesiredCapabilities capability = new DesiredCapabilities();
                capability.setBrowserName(DesiredCapabilities.internetExplorer().getBrowserName());
                capability.setVersion("8");
                capability.setPlatform(Platform.WINDOWS);
                capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                driver = new RemoteWebDriver(new URL(gridURL), capability);
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;
            default:
                throw new Exception("Browser type not supported");
            }

            data = new Properties();

            String propFileName = this.getClass().getSimpleName() + ".txt";
            String propPackage = this.getClass().getPackage().getName() + ".data";
            String fn = "/" + propPackage.replace(".", "/") + '/' + propFileName;

            baseLog.info("If data file exists, then it will be loaded.  Looking for file: " + fn);

            URL url = this.getClass().getResource(fn);
            if (null != url)
            {
                InputStream inputStream = url.openStream();

                if (inputStream != null)
                {
                    ((Properties) data).load(inputStream);
                    baseLog.info("Found data file, loaded " + data.size() + " item(s).");

                }
                else
                {
                    throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
                }
            }
            else
            {
                baseLog.info("No datafile found:" + fn);
            }

            getPropertyValue();
            getPropertyValueScreens();
            getNotificationProperties();
            getShipmentProperties();

            driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        }
        catch (Exception e)
        {
            baseLog.error(e.getMessage());
        }
        baseLog.info("Browser open");
    }

    public void login(String baseURL, String username) throws IllegalArgumentException
    {
        login(baseURL, username, null);
    }

    public void login(String baseURL, String username, String password) throws IllegalArgumentException
    {

        if (null == baseURL)
        {
            throw new IllegalArgumentException("baseURL must be set");
        }
        if (null == username)
        {
            throw new IllegalArgumentException("username must be set");
        }

        String creds;
        this.baseURL = baseURL;

        if (null == password)
        {
            creds = username;
        }
        else
        {
            creds = username + ":" + password;
        }

        String encode_creds = new String(java.util.Base64.getEncoder().encode(creds.getBytes()));

        getDriver().get(baseURL);
        if (browser == Browser.IE_REMOTE)
        { // Setting cookies in IE8 is broken,
          // using JavaScript:
            ((JavascriptExecutor) getDriver()).executeScript("document.cookie='username=" + encode_creds + "'");
        }
        else
        {
            Cookie ck = null;
            try
            {
                ck = new Cookie("username", encode_creds, new URL(baseURL).getHost(), "/", null, false);
            }
            catch (MalformedURLException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            getDriver().manage().addCookie(ck);
        }
    }

    public String getBaseURL()
    {
        return this.baseURL;
    }

    /**
     * Clean up.
     * 
     * @throws Exception
     *             when the driver fails to quit.
     */
    @After
    public void after() throws Exception
    {

        baseLog.info("============ Test Finished ============");
        try
        {
            if (null != driver)
            {
                driver.quit();
            }
        }
        catch (Exception e)
        {
            baseLog.error(e.getMessage());
        }
    }

    /**
     * Gets properties for menu items (xpaths).
     * 
     * @throws IOException
     */
    protected void getPropertyValue() throws IOException
    {
        pageToData = new Properties();
        String propertiesFileName = "menu.properties";
        baseLog.info("If data file exists, then it will be loaded.  Looking for file: " + propertiesFileName);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

        if (inputStream != null)
        {
            ((Properties) pageToData).load(inputStream);
            baseLog.info("Found data file, loaded " + pageToData.size() + " item(s).");
        }
        else
        {
            throw new FileNotFoundException("Property file '" + propertiesFileName + "' not found in classpath");
        }
    }

    /**
     * Gets screen header names with their screen numbers from a properties file.
     * 
     * @throws IOException
     */
    protected void getPropertyValueScreens() throws IOException
    {
        screenData = new Properties();
        String propertiesFileName = "screen.properties";
        baseLog.info("If data file exists, then it will be loaded.  Looking for file: " + propertiesFileName);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

        if (inputStream != null)
        {
            ((Properties) screenData).load(inputStream);
            baseLog.info("Found data file, loaded " + screenData.size() + " item(s).");
        }
        else
        {
            throw new FileNotFoundException("Property file '" + propertiesFileName + "' not found in classpath");
        }
    }

    /**
     * Gets notification id's, link texts and xpaths from a properties file.
     * 
     * @throws IOException
     */
    protected void getNotificationProperties() throws IOException
    {
        notificationData = new Properties();
        String propertiesFileName = "notification.properties";
        baseLog.info("If data file exists, then it will be loaded.  Looking for file: " + propertiesFileName);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

        if (inputStream != null)
        {
            ((Properties) notificationData).load(inputStream);
            baseLog.info("Found data file, loaded " + notificationData.size() + " item(s).");
        }
        else
        {
            throw new FileNotFoundException("Property file '" + propertiesFileName + "' not found in classpath");
        }
    }

    /**
     * Gets shipment id's, link texts and xpaths from a properties file.
     * 
     * @throws IOException
     */
    protected void getShipmentProperties() throws IOException
    {
        shipmentData = new Properties();
        String propertiesFileName = "shipment.properties";
        baseLog.info("If data file exists, then it will be loaded.  Looking for file: " + propertiesFileName);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

        if (inputStream != null)
        {
            ((Properties) shipmentData).load(inputStream);
            baseLog.info("Found data file, loaded " + shipmentData.size() + " item(s).");
        }
        else
        {
            throw new FileNotFoundException("Property file '" + propertiesFileName + "' not found in classpath");
        }
    }

    /**
     * @return The browser the test is using.
     */
    public Browser getBrowser()
    {
        return browser;
    }

}